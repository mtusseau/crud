<?php
namespace CRUD\Composer;

require_once __DIR__.'/vendor/autoload.php';

/**
 * Class Builder
 * Contains all built-in scripts method for current project.
 * @package CRUD\Composer
 */

class Builder
{
    /**
     * Build OpenAPI specifications file
     */
    public static function openapi()
    {
        $options = ['exclude' => 'vendor'];
        $openapi = \OpenApi\scan(__DIR__, $options);
        $openapi->saveAs(__DIR__.'/openapi.json');
    }
}