FROM php:7.2-apache

ARG BUILD_DATE
ARG COMMIT_SHA1

COPY . /var/www/html/

LABEL commit-sha1=${COMMIT_SHA1}

RUN a2enmod rewrite