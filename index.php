<?php
if (file_exists('c3.php')){
    include __DIR__.'/c3.php';
    define('MY_APP_STARTED', true);
}

include __DIR__ . '/CRUD/controllers/Owner.php';

require_once __DIR__.'/vendor/autoload.php';
use Luracast\Restler\Restler;

/**
 * @OA\Info(title="CRUD API", version="1.0")
 */

$r = new Restler();
$r->addAPIClass(\CRUD\Controller\Owner::class);
$r->handle();
