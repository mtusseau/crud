@ CRUD
Feature: Create Owners

  Scenario: Creating new Owner with JSON
    Given that I want to create a new "Owner"
    And his "name" is "Chris"
    And his "email" is "chris@world.com"
    When I request "/owner"
    Then I see response code is "200"
    And I see response is JSON
    And I see response has a "id" property
