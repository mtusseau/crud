<?php

use Codeception\Scenario;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;

    private $_restObject = null;
    private $_restObjectType = null;
    private $_restObjectMethod = null;

    public function __construct(Scenario $scenario)
    {
        $this->_restObject = new stdClass();
        $this->scenario = $scenario;
    }

    /**
     * @Given that I want to create a new :arg1
     */
    public function thatIWantToCreateANew($arg1)
    {
        $this->_restObjectType = ucwords(strtolower($arg1));
        $this->_restObjectMethod = 'post';
    }

    /**
     * @Given his :arg1 is :arg2
     */
    public function hisIs($arg1, $arg2)
    {
        $this->_restObject->$arg1 = $arg2;
    }

    /**
     * @When I request :arg1
     */
    public function iRequest($arg1)
    {
        switch (strtoupper($this->_restObjectMethod)) {
            case 'POST':
                $this->sendPOST($arg1, (array) $this->_restObject);
                break;
            default:
                throw new \PHPUnit\Framework\IncompleteTestError();
        }
    }

    /**
     * @Then I see response code is :num
     */
    public function iseeResponseCodeIs($num)
    {
        $this->seeResponseCodeIs($num);
    }

    /**
     * @Then I see response is JSON
     */
    public function theResponseShouldBeJSON()
    {
        $this->seeResponseIsJson();
    }

    /**
     * @Then I see response has a :arg1 property
     */
    public function theResponseHasAProperty($arg1)
    {
        $data = json_decode($this->grabResponse());
        if (!empty($data)) {
            if (!isset($data->$arg1)) {
                throw new Exception("Property '"
                    . $arg1 . "' is not set!\n\n"
                    . $data);
            }
        }
    }
}
