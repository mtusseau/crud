<?php

namespace CRUD\Controller;

include __DIR__ . '/../EntityManager.php';
include __DIR__ . '/../models/Owner.php';

/**
 * @OA\Schema(
 *     schema="NewOwner"
 * )
 */

class Owner {
    /**
     * @OA\Property(
     *     title="Owner name",
     *     description="Owner name"
     * )
     * @var string
     */
    private $name;

    /**
     * @OA\Property(
     *     title="Owner E-mail",
     *     description="Owner E-mail"
     * )
     * @var string
     */
    private $email;
    function __construct()
    {}

    /**
     * Insert new Owner entry in the database
     * @OA\Post(
     *     path="/owners",
     *     @OA\RequestBody(required=true,
     *                     description="The owner to add",
     *                     @OA\JsonContent(ref="#/components/schemas/NewOwner")),
     *     @OA\Response(response="200",
     *                  description="Successful creation",
     *                  @OA\JsonContent(ref="#/components/schemas/Owner")),
     *     @OA\Response(response="400",
     *                  description="Bad request. A field is missing.")
     * )
     * @param $request_data The request data
     */
    function post($request_data = NULL)
    {
        $owner = $this->_validate($request_data);
        \CRUD\EntityManager::getInstance()->persist($owner);
        \CRUD\EntityManager::getInstance()->flush();
        return $owner->toArray();
    }

    function index()
    {
        $r = array();
        foreach( \CRUD\EntityManager::getInstance()->getRepository('CRUD\\models\\Owner')->findAll() as $owner){
            array_push($r, $owner->toArray());
        }
        return $r;
    }
    function get($id)
    {
        return \CRUD\EntityManager::getInstance()->getRepository('CRUD\\models\\Owner')->find($id)->toArray();
    }

    private function _validate($data)
    {
        $owner = new \CRUD\models\Owner();
        if (!isset($data["name"])) {
            throw new RestException(400, "'name' field missing");
        }
        else {
            $owner->setName($data["name"]);
        }
        if (!isset($data["email"])) {
            throw new RestException(400, "'email' field missing");
        }
        else {
            $owner->setEmail($data["email"]);
        }
        return $owner;
    }
}