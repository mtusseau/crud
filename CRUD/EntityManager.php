<?php

namespace CRUD;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\Setup;

class EntityManager
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    private static $_instance = null;

    private function __construct()
    {
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */

    public static function getInstance() {

        if(is_null(self::$_instance)) {
            $isDevMode = true;
            $proxyDir = null;
            $cache = null;
            $useSimpleAnnotationReader = false;
            $paths = array(__DIR__);
            $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

            # OpenAPI annotations can be ignored for ORM purpose
            AnnotationReader::addGlobalIgnoredNamespace("OA");

            $connectionOptions = array(
                'driver'   => 'pdo_mysql',
                'host'     => 'mysql',
                'dbname'   => 'helloworld',
                'user'     => 'root',
                'password' => 'password'
            );

            self::$_instance = \Doctrine\ORM\EntityManager::create($connectionOptions, $config);
            $metadatas = self::$_instance->getMetadataFactory()->getAllMetadata();
            $st = new SchemaTool(self::$_instance);
            $st->updateSchema($metadatas);
        }
        return self::$_instance;
    }
}