<?php

namespace CRUD\models;

use Doctrine\ORM\Mapping as ORM;

/**
 *  Class Owner
 *  @package CRUD\models
 *  @OA\Schema(
 *      description="Owner",
 *     title="Owner"
 * )
 * @ORM\Entity
 * @ORM\Table(name="owners")
 */


class Owner
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     * )
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="Owner name",
     *     description="Owner name"
     * )
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $name;

    /**
     * @OA\Property(
     *     title="Owner E-mail",
     *     description="Owner E-mail"
     * )
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $email;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function toArray(){
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'email' => $this->getEmail()
        );
    }

}